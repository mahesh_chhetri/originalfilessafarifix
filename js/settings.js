"us strict";

/**
 * settings {Object} - All the settings
 * - firstNumberOfImages {Number} -  Total number of images which will be loaded on page load
 * - compareTo {Number}  -  Total number of clicks for image load. (3), one image will get load on 3 times clicks 
 */
var settings = {
    firstNumberOfImages: 15,
    compareTo: 3
}